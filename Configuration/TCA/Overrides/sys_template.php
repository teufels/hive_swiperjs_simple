<?php
defined('TYPO3') or die();

call_user_func(function()
{
    $extensionKey = 'hive_swiperjs_simple';

    /**
     * Default TypoScript
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'HIVE>Simple Slider'
    );
});