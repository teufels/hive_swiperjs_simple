<?php
defined('TYPO3_MODE') || die();

call_user_func(function () {

//Adding Custom CType Item Group
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItemGroup(
    'tt_content',
    'CType',
    'hive',
    'HIVE',
    'after:special'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['hiveswiperjssimple_hive_swiperjs_simple'] = 'tx_hiveswiperjssimple_hive_swiperjs_simple';
$tempColumns = [

    //items could not be override in thm_custom for own configuration (don't know why has worked)
    'tx_hiveswiperjssimple_configuration' => [
        'config' => [
            'renderType' => 'selectSingle',
            'type' => 'select',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_configuration',
    ],

    'tx_hiveswiperjssimple_sliderautoplay' => [
        'config' => [
            'default' => '0',
            'eval' => 'int',
            'type' => 'input',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'description' => 'Autoplay Delay in ms (0 = disabled)',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderautoplay',
    ],
    'tx_hiveswiperjssimple_sliderelement' => [
        'config' => [
            'appearance' => [
                'enabledControls' => [
                    'dragdrop' => '1',
                ],
                'levelLinksPosition' => 'both',
                'useSortable' => '1',
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_hiveswiperjssimple_sliderelement',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
            'minitems' => 1,
        ],
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderelement',
    ],
    'tx_hiveswiperjssimple_sliderhashnavigation' => [
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'default' => 0,
            'items' => [
                [
                    0 => '',
                    1 => '',
                ]
            ],
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'description' => 'Enables hash url navigation for slides',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderhashnavigation',
    ],
    'tx_hiveswiperjssimple_sliderloop' => [
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'default' => 0,
            'items' => [
                [
                    0 => '',
                    1 => '',
                ]
            ],
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'description' => 'Loop Mode / Infinite Loop',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderloop',
    ],
    'tx_hiveswiperjssimple_sliderpagination' => [
        'config' => [
            'items' => [
                [
                    'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderpagination.I.0',
                    'default',
                ],
                [
                    'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderpagination.I.1',
                    'dynamicbullets',
                ],
                [
                    'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderpagination.I.2',
                    'fraction',
                ],
                [
                    'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderpagination.I.3',
                    'progressbar',
                ],
                [
                    'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderpagination.I.4',
                    'custom',
                ],
                [
                    'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderpagination.I.5',
                    'nopagination',
                ],
            ],
            'renderType' => 'selectSingle',
            'type' => 'select',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_sliderpagination',
    ],
    'tx_hiveswiperjssimple_slidershowarrows' => [
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'default' => 1,
            'items' => [
                [
                    0 => '',
                    1 => '',
                ]
            ],
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'description' => 'show navigation arrows',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hiveswiperjssimple_slidershowarrows',
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:hive_swiperjs_simple/Resources/Private/Language/locallang_db.xlf:tt_content.CType.hiveswiperjssimple_hive_swiperjs_simple',
    'hiveswiperjssimple_hive_swiperjs_simple',
    // Icon
    'tx_hiveswiperjssimple_hive_swiperjs_simple',
    // The group ID
    'hive'
];
$tempTypes = [
    'hiveswiperjssimple_hive_swiperjs_simple' => [
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_hiveswiperjssimple_configuration,tx_hiveswiperjssimple_sliderpagination,tx_hiveswiperjssimple_sliderautoplay,tx_hiveswiperjssimple_slidershowarrows,tx_hiveswiperjssimple_sliderloop,tx_hiveswiperjssimple_sliderhashnavigation,tx_hiveswiperjssimple_sliderelement,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
    ],
];
$GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

});

