![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__swiperjs__simple-blue.svg)
![version](https://img.shields.io/badge/version-3.3.*-yellow.svg?style=flat-square)

HIVE Simple Slider
==========
A Simple Slider based on Swiper.js (6.8)

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)
![CUSTOMER](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

#### Composer support
`composer req beewilly/hive_swiperjs_simple`

## Swiper.js Documents
  * [Getting Started Guide](https://swiperjs.com/get-started/)
  * [API](https://swiperjs.com/api/)
  * [Demos](https://swiperjs.com/demos/)
  
## Suggested
- `"ichhabrecht/hide-used-content": "^1.0"`

## Migrations
from Version 1 to 2: migrate configuration 
  * Select Configuration / create and use own if so far js override was used

#### Notice
- developed with mask & mask_export
- in2code/hide-used-content fixes error that nested elements are shown as "unused"

## Changelog
- 3.3.7 FIX SQL error: 'Field 'tx_hiveswiperjssimple_configuration' doesn't have a default value'
- 3.3.6 remove unused mask config
- 3.3.5 prepare for TYPO3 v12 LTS
- 3.3.4 add id to next/prev/pagination    
- 3.3.1 fix wrong ordering & active state in custom pagination
- 3.3.0 TCA 'tx_hiveswiperjssimple_configuration' needed in EXT for PHP 8.1
- 3.2.0 PHP8 fix
- 3.1.0 implement 'HIVE' CType Group
- 3.0.0 no gulp Version
  - inlcude JS/SCSS self (nogulp needed)
  - remove support for Typo3 9.5 & 10.4
  - update to Swiper 6.8.4
- 2.1.* Fixes
- 2.1.0 Translation (L10n) Support & use master Branch
- 2.0.* Bug Fixes
- 2.0.0 add configuration to use own JS
- 1.0.0 inital