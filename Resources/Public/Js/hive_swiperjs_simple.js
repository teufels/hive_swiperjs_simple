$( document ).ready( function() {
    var aSwiper = [];
    var titles = [];

    $( ".tx_hive_swiperjs_simple .swiper-container[data-configuration='default']" ).each(function( index ) {

        // Initialize Configuration
        var $uid = $( this ).data("uid");
        var $autoplay = parseInt($( this ).data("autoplay"));
        var $loop = parseInt($( this ).data("loop"));
        var $hashNavigation = parseInt($( this ).data("hash-navigation"));
        if($hashNavigation == 1) { $hashNavigation = true; }
        var $paginationType = $( this ).data("pagination-type");

        // Initialize Swiper
        console.log("hive_swiperjs_simple :: Swiper initialize #hive-swiperjs-simple--" + $uid);
        var swiperIdentifier = '#hive-swiperjs-simple--' + $uid;
        aSwiper[index] = new Swiper('#hive-swiperjs-simple--' + $uid, {

            loop: $loop,

            hashNavigation: $hashNavigation,

            pagination: {
                //el: '#swiper-pagination--' + $uid,
                el: '.swiper-pagination',
                clickable: true,
            },

            navigation: {
                //nextEl: '#swiper-button-next--' + $uid,
                //prevEl: '#swiper-button-prev--' + $uid,
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });

        // update Swiper (add Params)
        //pagination
        switch ($paginationType) {
            case 'dynamicbullets':
                aSwiper[index].params.pagination.type = 'bullets';
                aSwiper[index].params.pagination.dynamicBullets = true;
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'fraction':
                aSwiper[index].params.pagination.type = 'fraction';
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'progressbar':
                aSwiper[index].params.pagination.type = 'progressbar';
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'custom':
                aSwiper[index].params.pagination.clickable = true;
                aSwiper[index].params.pagination.renderBullet = function (index, className) {
                    var el =  $( swiperIdentifier ).find(".slide--" + (index + 1) + ":not(.swiper-slide-duplicate)");
                    var titleValue = el.data("pagination-title").toString();
                    if (!titles.includes(titleValue)) {
                        titles.push(titleValue);
                        return '<span class="' + className + ' swiper-pagination-custom swiper-pagination-titled">' + titles[index] + '</span>';
                    }
                    return "";
                };
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'nopagination':
                aSwiper[index].pagination.destroy();
                break;
            default:
                aSwiper[index].params.pagination.type = 'bullets';
                aSwiper[index].params.pagination.dynamicBullets = false;
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
        }

        //autoplay
        if($autoplay > 0) {
            aSwiper[index].params.autoplay.delay = $autoplay;
            aSwiper[index].autoplay.start();
        }

        aSwiper[index].update();

    });

});