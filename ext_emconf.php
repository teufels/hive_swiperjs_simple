<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "hive_swiperjs_simple".
 *
 * Auto generated 06-10-2020 10:44
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'HIVE>Simple Slider',
    'description' => 'HIVE Simple Slider (based on Swiper.js)',
    'category' => 'plugin',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'state' => 'stable',
    'version' => '3.3.7',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-0.0.0'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

