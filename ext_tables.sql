CREATE TABLE tt_content (
    tx_hiveswiperjssimple_elementcontent_parent int(11) unsigned DEFAULT '0' NOT NULL,
    KEY tx_hiveswiperjssimple_elementcontent_parent (tx_hiveswiperjssimple_elementcontent_parent,pid,deleted),
    tx_hiveswiperjssimple_configuration tinytext DEFAULT 'default',
    tx_hiveswiperjssimple_sliderautoplay tinytext,
    tx_hiveswiperjssimple_sliderelement int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hiveswiperjssimple_sliderhashnavigation tinyint(4) DEFAULT '0' NOT NULL,
    tx_hiveswiperjssimple_sliderloop tinyint(4) DEFAULT '0' NOT NULL,
    tx_hiveswiperjssimple_sliderpagination tinytext,
    tx_hiveswiperjssimple_slidershowarrows tinyint(4) DEFAULT '0' NOT NULL
);
CREATE TABLE tx_hiveswiperjssimple_sliderelement (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    tx_hiveswiperjssimple_elementbackendtitle tinytext,
    tx_hiveswiperjssimple_elementbgimage int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hiveswiperjssimple_elementbgposition tinytext,
    tx_hiveswiperjssimple_elementcontent int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hiveswiperjssimple_elementcssclass tinytext,
    tx_hiveswiperjssimple_elementpaginationtitle tinytext,
    KEY language (l10n_parent,sys_language_uid)
);
